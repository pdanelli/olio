# frozen_string_literal: true

class ArticlesController < ApplicationController
  before_action :retrieve_remote_articles, only: :index

  protected

  def retrieve_remote_articles
    @articles = Olio::ArticlesApiReader.new.perform
  end
end
