# frozen_string_literal: true

class ArticleEngagementsController < ApplicationController
  def update
    # Ensure that the requested engagement type is valid before proceeding
    raise ActionController::NotImplemented, I18n.t("articles.engagement.not_implemented") unless valid_engagement?

    engagement.public_send(engagement_method)

    redirect_back fallback_location: root_path
  end

  protected

  # The engagement instance for this request
  #
  # @return [ArticleEngagement|ActiveRecord::RecordNotFound]
  def engagement
    @engagement ||= ArticleEngagement.find_by!(article_engagement_params)
  end

  # Is the engagement type requested availble on the engagement instance, ensuring that a user cannot call random
  # methods on the engagement instance
  #
  # @return [Boolean]
  def valid_engagement?
    engagement.respond_to?(engagement_method)
  end

  # Takes a plural engagment type and creates a singular "bang" (!) method name to be called on the engagement.
  # This means we could quickly add new engagement types without new routes or additional controller code, i.e. dislike.
  #
  # @return [Symbol]
  def engagement_method
    "#{engagement_type.singularize}!".to_sym
  end

  # The type of engagement being requested via the params
  #
  # @return [String]
  def engagement_type
    params.require(:engagement)
  end

  # Extract the article ID from the params, which can be passed directly to `find_by`
  #
  # @return [ActionController::Parameters]
  def article_engagement_params
    params.permit(:article_id)
  end
end
