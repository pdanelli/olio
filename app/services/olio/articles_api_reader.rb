# frozen_string_literal: true

# Makes a request to the articles API and extracts the result
#
# @usage Olio::ArticlesApiReader.new.perform
module Olio
  class ArticlesApiReader
    include ::HTTParty

    base_uri "https://s3-eu-west-1.amazonaws.com/"

    # Perform the request
    #
    # @return [Array] an empty array, or an array of Article instances
    def perform
      return [] unless response.success?

      # NOTE: It is possible to automatically parse the hash into an Article using `object_class: Article`,
      # however this also parses any nested hashes into Article instances, which is not what I want.
      JSON.parse(response.body).map { |item| Article.new(item) }
    end

    protected

    # Perform the GET request to the API endpoint
    #
    # @return [HTTParty::Response] the response from the API request, wrapped in an HTTParty::Response object
    def response
      @response ||= self.class.get("/olio-staging-images/developer/test-articles-v4.json")
    end
  end
end

