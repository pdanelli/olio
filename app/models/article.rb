# frozen_string_literal: true

class Article < OpenStruct
  # Return the engagement record for the article, or create one, with the current likes as a default value.
  #
  # NOTE: This isn't the most efficiant way of returning the likes for an article, as it will have a number of
  # issues such as n+1 queries, and needs to create a record for each article it finds on the first page load.
  # However, for the purposes of this test, it is a nice way of encapsulating the functionality. With more records,
  # or in a production system, the data would be best stored in Redis, or a single record that can be retreived with
  # a one query.
  def engagement
    @engagement ||= ArticleEngagement.find_or_create_by(article_id: id) { |ae| ae.likes = reactions["likes"] }
  end

  # Parse the created_at date for an article and return the format required
  #
  # @return [String]
  def date_added
    return "Unknown" if created_at.blank?

    created_at.to_time.strftime("%a %b %e %Y")
  end

  # Return the image SRC for an article
  #
  # @return [String]
  def image
    return "" if images.blank?

    images.first.dig("files", "medium")
  end

  # Allow us to use rails render methods without specifying the full path
  #
  # @return [String]
  def to_partial_path
    "articles/article"
  end
end
