# frozen_string_literal: true

class ArticleEngagement < ApplicationRecord
  validates_presence_of :article_id

  # Increment our likes for this engagement
  #
  # @return [void]
  def like!
    increment!(:likes)
  end
end
