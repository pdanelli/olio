# frozen_string_literal: true

RSpec.describe Article, type: :model do
  subject(:article) { described_class.new(article_data) }
  let(:article_data) { JSON.parse(fixture).first }
  let(:fixture) { File.read(Rails.root.join("spec", "fixtures", "articles", "request.json")) }

  describe "#engagement" do
    context "when the article doesn't have an engagement" do
      it "creates an engagement" do
        expect { article.engagement }.to change { ArticleEngagement.count }.by(1)
        expect(article.engagement.article_id.to_i).to eq article.id
        expect { article.engagement }.not_to change { ArticleEngagement.count }
      end
    end

    context "when the article has an existing engagement" do
      let!(:engagement) { ArticleEngagement.create(article_id: "3899631") }

      it "returns the correct engagement" do
        expect(article.engagement.article_id).to eq engagement.article_id
      end

      context "when there are exiting likes" do
        subject(:liked_article) { described_class.new(liked_article_data) }
        let(:liked_article_data) { article_data.merge!(new_data) }
        let(:new_data) { { "id" => id, "reactions" => { "likes" => like_count } } }
        let(:id) { "1234567890" }
        let(:like_count) { 7 }

        it "sets the ArticleEngagement likes" do
          expect(liked_article.engagement.likes.to_i).to eq like_count
          expect(liked_article.engagement.article_id).to eq id
        end
      end
    end
  end

  describe "#date_added" do
    it "formats the article date" do
      expect(article.date_added).to eq "Sat Dec 12 2020"
    end
  end

  describe "#image" do
    let(:src) { "https:\/\/cdn.olioex.com\/uploads\/photo\/file\/00gRGrBRDFYrR2j-9SJVYg\/medium_image.jpg" }
    it "returns the image src" do
      expect(article.image).to eq(src)
    end
  end
end
