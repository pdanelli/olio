# frozen_string_literal: true

RSpec.describe ArticleEngagement, type: :model do
  subject(:article_engagement) { described_class.create(article_id: "12345", likes: 0) }

  describe "validations" do
    subject(:article_engagement) { described_class.new }

    it "ensures the article article_id is set" do
      expect(article_engagement.valid?).to be_falsey
      expect(article_engagement.errors.full_messages).to eq ["Article can't be blank"]
    end
  end

  describe "#like!" do
    it "increments the like count" do
      expect { article_engagement.like! }.to change { article_engagement.likes }.from(0).to(1)
      expect { article_engagement.like! }.to change { article_engagement.likes }.from(1).to(2)
    end
  end
end
