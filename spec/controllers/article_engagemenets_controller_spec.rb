# frozen_string_literal: true

require "rails_helper"

RSpec.describe ArticleEngagementsController, type: :controller do
  let(:request_url) { "https://s3-eu-west-1.amazonaws.com/olio-staging-images/developer/test-articles-v4.json" }
  let(:response_json) { File.read(Rails.root.join("spec", "fixtures", "articles", "request.json")) }
  let(:response_headers) { { "content-type" => ["application/json; charset=utf-8"] } }
  let(:request_headers) do
    {
      headers: {
        "Accept" => "*/*",
        "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
        "User-Agent" => "Ruby"
      }
    }
  end
  let(:status_code) { 200 }
  let(:article_data) { JSON.parse(fixture).first }
  let(:fixture) { File.read(Rails.root.join("spec", "fixtures", "articles", "request.json")) }

  before do
    stub_request(:get, request_url).with(request_headers).to_return(
      status: status_code,
      body: response_json,
      headers: response_headers
    )

    ArticleEngagement.create(article_id: article_data["id"])
  end

  describe "#PUT update" do
    let(:engagement) { "likes" }

    context "when the engagement is valid" do
      before do
        put :update, params: { article_id: article_data["id"], engagement: engagement}
      end

      it "returns a 200 status" do
        expect(response).to have_http_status(:found)
      end
    end

    context "when the engagement is invalid" do
      let(:engagement) { "dislikes" }

      it "returns a 200 status" do
        expect { put :update, params: { article_id: article_data["id"], engagement: engagement} }
          .to raise_error(ActionController::NotImplemented)
      end
    end
  end
end

