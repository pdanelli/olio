# frozen_string_literal: true

require "rails_helper"

RSpec.describe ArticlesController, type: :controller do
  let(:request_url) { "https://s3-eu-west-1.amazonaws.com/olio-staging-images/developer/test-articles-v4.json" }
  let(:response_json) { File.read(Rails.root.join("spec", "fixtures", "articles", "request.json")) }
  let(:response_headers) { { "content-type" => ["application/json; charset=utf-8"] } }
  let(:request_headers) do
    {
      headers: {
        "Accept" => "*/*",
        "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
        "User-Agent" => "Ruby"
      }
    }
  end
  let(:status_code) { 200 }

  before do
    stub_request(:get, request_url).with(request_headers).to_return(
      status: status_code,
      body: response_json,
      headers: response_headers
    )
  end

  describe "#GET index" do
    before do
      get :index
    end

    it "returns a 200 status" do
      expect(response).to have_http_status(:ok)
    end
  end
end

