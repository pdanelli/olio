# frozen_string_literal: true

RSpec.describe Olio::ArticlesApiReader do
  let(:api_reader) { described_class.new }
  let(:request_url) { "https://s3-eu-west-1.amazonaws.com/olio-staging-images/developer/test-articles-v4.json" }
  let(:response_json) { File.read(Rails.root.join("spec", "fixtures", "articles", "request.json")) }
  let(:response_headers) { { "content-type" => ["application/json; charset=utf-8"] } }
  let(:status_code) { 200 }
  let(:request_headers) do
    {
      headers: {
        "Accept" => "*/*",
        "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
        "User-Agent" => "Ruby"
      }
    }
  end

  before do
    stub_request(:get, request_url).with(request_headers).to_return(
      status: status_code,
      body: response_json,
      headers: response_headers
    )
  end

  it "includes HTTParty" do
    expect(described_class.ancestors).to include(HTTParty)
  end

  describe "#perform" do
    subject(:result) { api_reader.perform }

    context "when the request is successful" do
      it "returns an array of Article instances" do
        expect(result).to be_a(Array)
        expect(result.count).to eq 1
        expect(result.first).to be_a(Article)
      end
    end

    context "when the request fails" do
      let(:status_code) { 500 }

      it "returns an array" do
        expect(result).to be_a(Array)
        expect(result).to be_empty
      end
    end
  end

  describe "#response" do
    it "returns the correct response type" do
      expect(api_reader.send(:response)).to be_a(HTTParty::Response)
    end
  end
end

