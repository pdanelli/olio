# frozen_string_literal: true

RSpec.describe "Liking an article", type: :feature do
  let(:request_url) { "https://s3-eu-west-1.amazonaws.com/olio-staging-images/developer/test-articles-v4.json" }
  let(:response_json) { File.read(Rails.root.join("spec", "fixtures", "articles", "request.json")) }
  let(:response_headers) { { "content-type" => ["application/json; charset=utf-8"] } }
  let(:request_headers) do
    {
      headers: {
        "Accept" => "*/*",
        "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
        "User-Agent" => "Ruby"
      }
    }
  end
  let(:status_code) { 200 }

  before do
    stub_request(:get, request_url).with(request_headers).to_return(
      status: status_code,
      body: response_json,
      headers: response_headers
    )
  end

  context "when visiting the application" do
    before do
      visit "/"
    end

    it "shows the article" do
      expect(page).to have_content I18n.t("articles.heading")
      expect(page).to have_selector(".article", count: 1)

      within(".article") do
        expect(page).to have_text("Ambipur air freshener plugin")
        expect(page).to have_text("Device only but refills are available most places")
        expect(page.find(".like-count")).to have_text("0")
      end
    end

    context "liking an article" do
      before do
        within(".article") do
          click_button
        end
      end

      it "shows the article with an updated like count" do
        within(".article") do
          expect(page).to have_text("Ambipur air freshener plugin")
          expect(page.find(".like-count")).to have_text("1")
        end
      end
    end
  end
end

