# Olio Technical Evaluation

## Introduction

Hello, this is Olio technical evaluation for Paul Danelli.

In order to maintain focus on the quality and extendability of the backend code, I have avoided complicating the application too many with frontend additions, just simple CSS.

## Installation

The application is self contained within the docker containers and has no external host dependencies. To run the application, please execute the following command in your terminal:

```bash
docker-compose up web
```

The container will then be built and install any Ruby Gem dependencies before starting the Rails server. To view the application please go to `http://localhost:3000` in your browser.

## Testing

To run the tests, execute the following command:

```bash
docker-compose run --rm runner rspec
```

