FROM ruby:3.0.3-slim-bullseye

# Build essentials
RUN apt-get update && \
	DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
		software-properties-common \
		build-essential \
		libsqlite3-dev \
		curl

# Clean up
RUN apt-get clean && \
		rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/* && \
		truncate -s 0 /var/log/*log

WORKDIR /usr/src/app

ENV LANG=C.UTF-8 \
		BUNDLE_JOBS=4 \
		BUNDLE_RETRY=3

# Install gem dependencies
COPY Gemfile* ./
RUN gem update --system && \
	gem install bundler && \
	bundle install

COPY . .

