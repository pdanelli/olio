class CreateArticleEngagements < ActiveRecord::Migration[7.0]
  def change
    create_table :article_engagements do |t|
      t.string :article_id
      t.integer :likes, default: 0

      t.timestamps
    end
  end
end
