Rails.application.routes.draw do
  root "articles#index"

  # ArticleEngagement entries are created on page load, so we would always be updating them
  put "articles/engagement", to: "article_engagements#update", as: :article_engagements
end
